@extends('layouts.app')

@section('styles')
<style>
    #dataTable_paginate{
        float: right;
        padding-right: 5px;
    }

    .dataTables_filter{
        float: right;
        padding-right: 5px;
    }
</style>
@endsection

@section('title'){{'Employee Salaries'}}@endsection

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Employee Salaries</h1>
    </div>

    @include('common.alert')
   
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">All Salaries</h6>
            
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Daily Salary</th>
                            <th>Total Salary</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Daily Salary</th>
                            <th>Total Salary</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @forelse ($employee_salaries as $employee_salary)
                            <tr>
                                <td>{{$employee_salary->employee->name}}</td>
                                <td>{{\Carbon\Carbon::parse($employee_salary->start_date)->format('d-m-Y')}}</td>
                                <td>{{\Carbon\Carbon::parse($employee_salary->end_date)->format('d-m-Y')}}</td>
                                <td>$ {{$employee_salary->employee->daily_salary}}</td>
                                <td>$ {{$employee_salary->total_salary}}</td>
                                <td>
                                    <a target="_blank" href="{{route('salary.pdf',['employee_salary_id' => $employee_salary->id])}}" class="btn btn-success"> View PDF</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No Employee Salaries Found</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')
<script src="{{asset('admin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('admin/js/datatables-demo.js')}}"></script>
@endsection