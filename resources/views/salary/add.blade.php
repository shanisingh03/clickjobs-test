@extends('layouts.app')

@section('styles')
    
@endsection

@section('title'){{'Add Salary'}}@endsection

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Add Salary</h1>
        <a href="{{route('employee.list')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-arrow-left fa-sm text-white-50"></i> Back</a>
    </div>

    @include('common.alert')
   
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Add New Salary</h6>
            
        </div>
        <div class="card-body">
            <form class="user" method="POST" action="{{route('salary.store')}}">
                
                @csrf
                <input type="hidden" name="employee_id" value="{{$employee->id}}" />
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <span style="color:red;">*</span>Name</label>
                        <p>{{$employee->name}}</p>
                    </div>
                    <div class="col-sm-6">
                        <span style="color:red;">*</span>Email</label>
                        <p>{{$employee->email}}</p>
                    </div>
                </div>
                <div class="form-group row">
                    
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <span style="color:red;">*</span>Date Of Birth</label>
                        <p>{{\Carbon\Carbon::parse($employee->dob)->format('d-m-Y')}}</p>
                    </div>

                    <div class="col-sm-6">
                        <span style="color:red;">*</span>Daily Salary</label>
                        <p>$ {{$employee->daily_salary}}</p>
                    </div>
                </div>
                <div class="form-group">
                    <span style="color:red;">*</span>Address</label>
                    <p>{{$employee->address}}</p>
                </div>

                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <span style="color:red;">*</span>Start Date</label>
                        <input type="date" class="form-control form-control-user @error('start_date') is-invalid @enderror"
                        name="start_date" value="{{ old('start_date') }}" required placeholder="Start Date" id='startDate'>
                        @error('start_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-sm-6">
                        <span style="color:red;">*</span>End Date</label>
                        <input type="date" class="form-control form-control-user @error('end_date') is-invalid @enderror"
                        name="end_date" value="{{ old('end_date') }}" required placeholder="End Date" id='startDate'>
                        @error('end_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block">
                    Create Salary
                </button>
            </form>
        </div>
    </div>

</div>
@endsection

@section('scripts')

@endsection
