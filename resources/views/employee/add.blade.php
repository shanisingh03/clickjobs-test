@extends('layouts.app')

@section('styles')
    
@endsection

@section('title'){{'Add Users'}}@endsection

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Add Users</h1>
        <a href="{{route('employee.list')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-arrow-left fa-sm text-white-50"></i> Back</a>
    </div>

    @include('common.alert')
   
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Add New User</h6>
            
        </div>
        <div class="card-body">
            <form class="user" method="POST" action="{{route('employee.store')}}">
                
                @csrf

                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <span style="color:red;">*</span>Name</label>
                        <input type="text" class="form-control form-control-user @error('name') is-invalid @enderror" id="exampleName"
                            placeholder="Name" required name="name" value="{{ old('name') }}">
                        
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-sm-6">
                        <span style="color:red;">*</span>Email</label>
                        <input type="email" id="email" class="form-control form-control-user @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required id="exampleEmail" placeholder="Email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <span style="color:red;">*</span>Date Of Birth</label>
                        <input type="date" class="form-control form-control-user @error('dob') is-invalid @enderror"
                        name="dob" value="{{ old('dob') }}" required placeholder="Date Of Birth" id='datetimepicker1'>
                        @error('dob')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-sm-6">
                        <span style="color:red;">*</span>Daily Salary</label>
                        <input type="number" class="form-control form-control-user @error('daily_salary') is-invalid @enderror" id="exampleSalary"
                        name="daily_salary" value="{{ old('daily_salary') }}" required id="exampledaily_salary" placeholder="Daily Salary">
                        @error('daily_salary')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <span style="color:red;">*</span>Address</label>
                    <input type="text" class="form-control form-control-user @error('address') is-invalid @enderror" id="exampleInputAddress"
                        placeholder="Address" name="address" value="{{ old('address') }}" required >
                    @error('address')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block">
                    Register Account
                </button>
            </form>
        </div>
    </div>

</div>
@endsection

@section('scripts')

@endsection
