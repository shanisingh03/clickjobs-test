@extends('layouts.app')

@section('styles')
<style>
    #dataTable_paginate{
        float: right;
        padding-right: 5px;
    }

    .dataTables_filter{
        float: right;
        padding-right: 5px;
    }
</style>
@endsection

@section('title'){{'Users List'}}@endsection

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Users</h1>
        <a href="{{route('employee.add')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-plus fa-sm text-white-50"></i> Add New</a>
    </div>

    @include('common.alert')
   
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">All Users</h6>
            
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>DOB</th>
                            <th>Daily Salary</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>DOB</th>
                            <th>Daily Salary</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @forelse ($employees as $employee)
                            <tr>
                                <td>{{$employee->name}}</td>
                                <td>{{$employee->email}}</td>
                                <td>{{\Carbon\Carbon::parse($employee->dob)->format('d-m-Y')}}</td>
                                <td>$ {{$employee->daily_salary}}</td>
                                <td>
                                    <a href="{{route('salary.create',['employee_id' => $employee->id])}}" class="btn btn-success"> Create Salary</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">No Employees Found</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')
<script src="{{asset('admin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('admin/js/datatables-demo.js')}}"></script>
@endsection