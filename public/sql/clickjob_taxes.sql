# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.32)
# Database: clickjobs
# Generation Time: 2021-04-09 18:04:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table click_job_taxes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `click_job_taxes`;

CREATE TABLE `click_job_taxes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `konto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factor` decimal(10,3) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `click_job_taxes` WRITE;
/*!40000 ALTER TABLE `click_job_taxes` DISABLE KEYS */;

INSERT INTO `click_job_taxes` (`id`, `konto`, `Description`, `factor`, `created_at`, `updated_at`)
VALUES
	(1,'1500','AHV',5.125,NULL,NULL),
	(2,'1600','ALV',1.100,NULL,NULL),
	(3,'1700','UVG',2.360,NULL,NULL),
	(4,'1800','BVG By Daily Salary Type',4.500,NULL,NULL),
	(5,'1900','KTG',0.280,NULL,NULL),
	(6,'2000','TAX (BON)',18.140,NULL,NULL),
	(7,'2100','Berufsbeitrag GAV Personal Welth',1.200,NULL,NULL);

/*!40000 ALTER TABLE `click_job_taxes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
