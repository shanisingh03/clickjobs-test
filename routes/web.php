<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FbController;
use App\Http\Controllers\SalaryController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes(['register' => false]);

Route::get('auth/facebook', [FbController::class, 'redirectToFacebook']);

Route::get('auth/facebook/callback', [FbController::class, 'facebookSignin']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Employees CRUD
Route::middleware('auth')->prefix('employee')->name('employee.')->group(function(){
    Route::get('list',[EmployeeController::class, 'getEmployeeList'])->name('list');
    Route::get('add',[EmployeeController::class, 'addEmployee'])->name('add');
    Route::post('store',[EmployeeController::class, 'storeEmployee'])->name('store');
    Route::get('edit/{employee_id}',[EmployeeController::class, 'editEmployee'])->name('edit');
    Route::post('update',[EmployeeController::class, 'updateEmployee'])->name('update');
    Route::post('delete',[EmployeeController::class, 'deleteEmployee'])->name('delete');
});


// Salary Create
Route::middleware('auth')->prefix('salary')->name('salary.')->group(function(){
    Route::get('create/{employee_id}',[SalaryController::class, 'createSalary'])->name('create');
    Route::post('store',[SalaryController::class, 'storeSalary'])->name('store');
    Route::get('list',[SalaryController::class, 'getSalaryList'])->name('list');
    Route::get('download-pdf/{employee_salary_id}',[SalaryController::class, 'getSalaryPdf'])->name('pdf');

});
