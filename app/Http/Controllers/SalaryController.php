<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Employee;
use PDF;
use App\Models\ClickJobTax;
use Illuminate\Http\Request;
use App\Models\EmployeeSalary;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\Http\Requests\StoreSalaryRequest;

class SalaryController extends Controller
{
    /**
     * Create Salary
     * @param Integer $employee_id
     * @return View with Employee Detail
     * @author Shani Singh
     */
    public function createSalary($employee_id)
    {
        try {
            $employee = Employee::whereId($employee_id)->first();

            if(empty($employee)){
                return redirect()->back()->with('error','Invalid Employee');
            }

            return View::make('salary.add')->with('employee', $employee);
        } catch (\Throwable $th) {
            return redirect()->back()->with('error',$th->getMessage());
        }
    }

    /**
     * Store Salary
     * @param StoreSalaryRequest $request
     * @return View with Employee Detail
     * @author Shani Singh
     */
    public function storeSalary(StoreSalaryRequest $request)
    {
        DB::beginTransaction();
        try {
            $start_date = Carbon::parse($request->start_date);
            $end_date = Carbon::parse($request->end_date);
            $days = $end_date->diffInDays($start_date);

            $employee = Employee::whereId($request->employee_id)->first();

            $save_salary = EmployeeSalary::create([
                'employee_id' => $employee->id,
                'start_date'    => $request->start_date,
                'end_date'    => $request->end_date,
                'days'    => $days,
                'total_salary'    => $days * $employee->daily_salary,
            ]);
            
            DB::commit();
            return redirect()->route('salary.list')->with('success', 'Salary Added successfully.');
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with('error',$th->getMessage());
        }
    }

    /**
     * Get Salary List
     * @param Nill
     * @return View $employee_salaries
     * @author Shani Singh
     */
    public function getSalaryList()
    {
        try {
            $employee_salaries = EmployeeSalary::with('employee')->get();

            return View::make('salary.list')->with('employee_salaries', $employee_salaries);

        } catch (\Throwable $th) {
            return redirect()->back()->with('error',$th->getMessage());
        }
    }

    public function getSalaryPdf($employee_salary_id)
    {
        try {
            $employee_salary = EmployeeSalary::whereId($employee_salary_id)->with('employee')->first();

            if(empty($employee_salary)){
                return redirect()->back()->with('error','Invalid Salary Id');
            }

            $taxes = ClickJobTax::all();

            $pdf = PDF::loadView('salary.invoice', [
                'employee_salary' => $employee_salary,
                'taxes'         => $taxes
            ]);

            return $pdf->stream('invoice.pdf');

        } catch (\Throwable $th) {
            return redirect()->back()->with('error',$th->getMessage());
        }
    }
}
