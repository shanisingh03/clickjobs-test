<?php

namespace App\Http\Controllers;
use Exception;

use Validator;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class FbController extends Controller
{
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function facebookSignin()
    {
        try {
    
            $user = Socialite::driver('facebook')->user();
            $facebookId = User::where('facebook_id', $user->id)->first();
            // dd($user);
            if($facebookId){
                Auth::login($facebookId);
                return redirect()->route('home');
            }else{
                $createUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email ? $user->email : $user->name.'_'.$user->id.'@test.com',
                    'facebook_id' => $user->id,
                    'password' => Hash::make('Admin!#123')
                ]);
    
                Auth::login($createUser);
                return redirect()->route('home');
            }
    
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
}