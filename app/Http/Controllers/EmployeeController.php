<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\Http\Requests\EmployeeStoreRequest;

class EmployeeController extends Controller
{
    /**
     * List Employee
     * @param Nill
     * @return View With $employees
     * @author Shani Singh
     */
    public function getEmployeeList()
    {
        try {
            $employees = Employee::all();

            return View::make('employee.list')->with('employees', $employees);

        } catch (\Throwable $th) {
            return redirect()->back()->with('error',$th->getMessage());
        }
    }
    /**
     * Add Employee
     * @param Nill
     * @return View Add Employee
     * @author Shani Singh
     */
    public function addEmployee()
    {
        try {
            
            return View::make('employee.add');

        } catch (\Throwable $th) {
            return redirect()->back()->with('error',$th->getMessage());
        }
    }

    /**
     * Edit Employee
     * @param Integer $employee_id
     * @return View With $employees
     * @author Shani Singh
     */
    public function editEmployee()
    {
        try {
            return View::make('employee.edit');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error',$th->getMessage());
        }
    }

    /**
     * Store Employee
     * @param EmployeeStoreRequest $request
     * @return View With $employees & Success Message
     * @author Shani Singh
     */
    public function storeEmployee(EmployeeStoreRequest $request)
    {
        DB::beginTransaction();
        try {

            $create_employee = Employee::create($request->all());

            if($create_employee){

                DB::commit();
                return redirect()->route('employee.list')->with('success', 'Employee Added Successfully.');
            }else{
                DB::rollBack();
                return redirect()->back()->with('error', 'Unable To Create Employee, Please try again');
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with('error',$th->getMessage());
        }
    }

    /**
     * Update Employee
     * @param EmployeeUpdateRequest $request
     * @return View With $employees & Success Message
     * @author Shani Singh
     */
    public function updateEmployee()
    {
        try {
            //code...
        } catch (\Throwable $th) {
            return redirect()->back()->with('error',$th->getMessage());
        }
    }

    /**
     * Delete Employee
     * @param EmployeeDeleteRequest $request
     * @return View With $employees & Success Message
     * @author Shani Singh
     */
    public function deleteEmployee()
    {
        try {
            //code...
        } catch (\Throwable $th) {
            return redirect()->back()->with('error',$th->getMessage());
        }
    }
}
